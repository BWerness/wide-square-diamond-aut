int k = 2;
int l = 9;
float lambda = 0.5;

int[] rule = new int[4*k*k+1];

int[][] univ = new int[2*k][2*k];

int[][] expandUniv(int[][] in) {
  int[][] out = new int[2*in.length-1][2*in.length-1];
  // first copy over ones that were in the original
  for (int i = 0; i < in.length; i++) {
    for (int j = 0; j < in.length; j++) {
      out[2*i][2*j] = in[i][j];
    } 
  }
  // now do the ones inbetween by corner
  for (int i = 1; i < out.length; i += 2) {
    for (int j = 1; j < out.length; j += 2) {
      if ((i-2*k+1 < 0) || (j-2*k+1 < 0) || (i+2*k-1 > out.length-1) || (j+2*k-1 > out.length-1)) {
        out[i][j] = int(random(2));  
      }
      else {
        int tot = 0;
        for (int di = -2*k+1; di <= 2*k-1; di += 2) {
          for (int dj = -2*k+1; dj <= 2*k-1; dj += 2) {
            tot += out[i+di][j+dj];
          } 
        }
        out[i][j] = rule[tot];
      }
    }
  }
  // now do the other phase of them
  for (int i = 0; i < out.length; i++) {
    for (int j = 0; j < out.length; j++) {
      if ((i+j)%2 == 1) {
        if ((i-2*k+1 < 0) || (j-2*k+1 < 0) || (i+2*k-1 > out.length-1) || (j+2*k-1 > out.length-1)) {
          out[i][j] = int(random(2));  
        }
        else {
          int tot = 0;
          for (int di = 0; di < 2*k; di ++) {
            for (int dj = 0; dj < 2*k; dj ++) {
              tot += out[i+di+dj-2*k+1][j+di-dj];
            } 
          }
          out[i][j] = rule[tot];
        }
      }
    }
  }
  return out;
}

void randomRule() {
  lambda = random(1.0);
  for (int i = 0; i < 4*k*k+1; i++) {
    rule[i] = random(1.0) < lambda ? 1:0;  
  }
}

void symmetricRule() {
  lambda = random(1.0);
  for (int i = 0; i < 4*k*k+1; i++) {
    rule[i] = random(1.0) < lambda ? 1:0;
    rule[4*k*k-i] = 1-rule[i];
  }
}

void intervalRule() {
  for (int i = 0; i < 4*k*k+1; i++) {
    rule[i] = 0;  
  }
  int i1 = int(random(4*k*k+1));
  int i2 = int(random(4*k*k+1));
  for (int i = min(i1,i2); i <= max(i1,i2); i++) {
    rule[i] = 1;  
  }
}

void densityRule() {
  for (int i = 0; i < 4*k*k+1; i++) {
    float d = (1.0*i)/(4*k*k);
    //d = 3*d*d-2*d*d; // can put any function here
    d = 1-d; // can put any function here
    rule[i] = random(1.0) < (1.0*i)/(4*k*k) ? 1:0;  
  }
}

void initUniv() {
  univ = new int[2*k][2*k];
  for (int i = 0; i < 2*k; i++) {
    for (int j = 0; j < 2*k; j++) {
      univ[i][j] = int(random(2));
    }
  }
}

void settings() {
  int s = 2*k;
  for (int i = 0; i < l; i++) {
    s = s*2-1;  
  }
  size((s+1)/2,(s+1)/2);
  pixelDensity(2);
}

void setup() {  
  randomRule();
  initUniv();
  
  noLoop();
}

void expandBy(int in) {
  for (int i = 0; i < in; i++) {
    univ = expandUniv(univ);
  }
}

void keyPressed() {
  switch (key) {
    case ' ': univ = expandUniv(univ); break;
    case 'r': initUniv(); break;
    case 'u': randomRule(); initUniv(); break;
    case 'a': randomRule(); initUniv(); expandBy(l); break;
    case 's': initUniv(); expandBy(l); break;
    case 'd': intervalRule(); initUniv(); expandBy(l); break;
    case 'f': densityRule(); initUniv(); expandBy(l); break;
    case 'h': symmetricRule(); initUniv(); expandBy(l); break;
    case 'p': print(univ.length);
    case 'v': save(k+"-"+ruleString()+"-"+int(random(10000))+".png");
  }
  redraw();
}

String ruleString() {
  String out = "";
  int acc = 0;
  int phase = 0;
  for (int i = 0; i < 4*k*k+1; i++) {
    acc = 2*acc + rule[i];
    phase++;
    if (phase == 3) {
      phase = 0;
      switch (acc) {
        case 10: out += "a"; break;
        case 11: out += "b"; break;
        case 12: out += "c"; break;
        case 13: out += "d"; break;
        case 14: out += "e"; break;
        case 15: out += "f"; break;
        default: out += acc; break;
      }
      acc = 0;
    }
  }
  if (phase != 0) {
    switch (acc) {
      case 10: out += "a"; break;
      case 11: out += "b"; break;
      case 12: out += "c"; break;
      case 13: out += "d"; break;
      case 14: out += "e"; break;
      case 15: out += "f"; break;
      default: out += acc; break;
    }
  }
  return out;
}

void draw() {
  loadPixels();
  for (int i = 0; i < min(univ.length,pixelWidth); i++) {
    for (int j = 0; j < min(univ.length,pixelWidth); j++) {
      pixels[i+pixelWidth*j] = color(255*univ[i][j]);
    }
  }
  updatePixels();
}